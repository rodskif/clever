<?php
get_header();
?>

<?php if (have_posts()) { ?>
    <?php
        the_post()
    ?>
<div class="gdlr-page-title-wrapper instructor">
        <div class="gdlr-page-title-overlay"></div>
        <div class="gdlr-page-title-container container" >
            <h1 class="gdlr-page-title page-header"><?php the_title(); ?></h1>
        </div>  
</div>
<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-md-12">
        <a href="#">&laquo;All Events</a>
            <h2 class="page-header">
                <?php the_title(); ?>
                <!-- <small>by <a href="#"><?php the_author(); ?></a>
                </small> -->
            </h2>
            <ol class="breadcrumb">
                <li><a href="/">Home</a>
                </li>
                <li class="active">Blog Post</li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <!-- Content Row -->
    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-md-12">

            <!-- Blog Post -->

            <hr>

            <!-- Date/Time -->
            <p><i class="fa fa-clock-o"></i> Опубликованно <?php the_time('j F Y'); ?> <!-- //с помощью формата j F Y --></p>
            <!-- <p><i class="fa fa-clock-o"></i> Опубликованно <?php the_date(); ?> // с помощью хука</p> -->
            <hr>

            <!-- Preview Image -->
            <?php 
                $postid = get_post_meta($post->ID, '_img_thumbnail', true);
                $img = wp_get_attachment_image_src($postid, 'full');
            ?>
            <center><img src="<?php echo $img[0] ?>" widht="<?php echo $img[1] ?>" alt=""></center>
            <!-- <img class="img-responsive" src="http://placehold.it/900x300" alt=""> -->

            <hr>

            <!-- Post Content -->

            <p class="lead">
                <?php
                    the_content();
                ?>

            </p>
            <hr>
            <!-- <p class="lead">
                <?php
                the_excerpt();
                ?>
            </p>
            <hr> -->



            
<?php } ?>

<?php
get_footer();
?>
