<?php
	add_action( 'admin_init', 'theme_options_init' );
	add_action( 'admin_menu', 'theme_options_add_page' );

	function theme_options_init(){
	register_setting( 'wpuniq_options', 'wpuniq_theme_options');
	}

	function theme_options_add_page() {
	add_theme_page( __( 'Настройки Темы', 'clever' ), __( 'Настройки Темы Clever', 'clever' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
	}
	function theme_options_do_page() { global $select_options; if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false;
?>
<div class="container">
	<?php screen_icon(); echo "<h2>". __( 'Настройки Темы', 'clever' ) . "</h2>"; ?>
	<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
	<div id="message" class="updated">
		<p><strong><?php _e( 'Настройки сохранены', 'clever' ); ?></strong></p>
	</div>
	<?php endif; ?>
</div>

<form method="post" action="options.php">
<?php settings_fields( 'wpuniq_options' ); ?>
<?php $options = get_option( 'wpuniq_theme_options' ); ?>
	<table width="600" border="0">
		<tr>
			<td>Logo:</td>
			<td><input type="file" name="file" /></td>
		</tr>
		<tr>
			<td>Телефон:</td>
			<td><input type="phone" name="wpuniq_theme_options[phone]" id="wpuniq_theme_options[phone]" value="<?php echo $options[phone];?>"></td>
		</tr>
		<tr>
			<td>E-mail:</td>
			<td><input type="email" name="wpuniq_theme_options[email]" id="wpuniq_theme_options[email]" value="<?php echo $options[email];?>"></td>
		</tr>
		<tr>
			<td>Показывать панель поиска?</td>
			<td><input type="checkbox" name="wpuniq_theme_options[show_search]" id="wpuniq_theme_options[show_search]" value="1"<?php if($options[show_search]=='1') echo ' checked="checked"';?>></td>
		</tr>
		<tr>
			<td>Показывать панель твитера?</td>
			<td><input type="checkbox" name="wpuniq_theme_options[show_twit]" id="wpuniq_theme_options[show_twit]" value="1"<?php if($options[show_twit]=='1') echo ' checked="checked"';?>></td>
		</tr>
		<tr>
			<td colspan="2"><a href="https://www.slickremix.com/docs/how-to-get-api-keys-and-tokens-for-twitter/">Инструкция: где взять ключи</td>
		</tr>
		<tr>
			<td>Key1:</td>
			<td><input type="text" name="wpuniq_theme_options[key1]" id="wpuniq_theme_options[key1]" value="<?php echo $options[key1];?>"></td>
		</tr>
		<tr>
			<td>Key2:</td>
			<td><input type="text" name="wpuniq_theme_options[key2]" id="wpuniq_theme_options[key2]" value="<?php echo $options[key2];?>"></td>
		</tr>
		<tr>
			<td>Key3:</td>
			<td><input type="text" name="wpuniq_theme_options[key3]" id="wpuniq_theme_options[key3]" value="<?php echo $options[key3];?>"></td>
		</tr>
		<tr>
			<td>Key4:</td>
			<td><input type="text" name="wpuniq_theme_options[key4]" id="wpuniq_theme_options[key4]" value="<?php echo $options[key4];?>"></td>
		</tr>
		<tr>
			<td>Twitter login:</td>
			<td><input type="text" name="wpuniq_theme_options[login]" id="wpuniq_theme_options[login]" value="<?php echo $options[login];?>"></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Применить" /></td>
		</tr>
	</table>
</form>

<?php
} 
?>