<?php
/*
Template Name: Шаблон страницы записей
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) { ?>

    <?php

        the_post()

    ?>
    <main id="main">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="gdlr-page-title-wrapper instructor">
                        <div class="gdlr-page-title-overlay"></div>
                        <div class="gdlr-page-title-container container" >
                            <h1 class="gdlr-page-title"><?php the_title(); ?></h1>
                        </div>  
                    </div>

                    <hr>

                    <?php get_template_part( 'loop' ); ?>

                </div>

            </div>

        </div>

    </main>
    <?php wp_reset_postdata(); } ?>

<?php get_footer(); ?>