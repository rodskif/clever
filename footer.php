<!-- Footer -->

<footer>
	<div class="footer-top" style="background-color: #262626">
		<div class="container">
			<div class="row">
				<?php dynamic_sidebar('footer'); ?>
			</div>
		</div>
	</div>
	<div class="footer-bottom" style="background-color: #000000">
		<div class="container">
		    <div class="row">

		        <div class="col-lg-12">

		            <p>Copyright &copy; <?php echo get_bloginfo('name').' '.date('Y'); ?></p>

		        </div>
		    </div>
		</div>	
	</div>
</footer>


<? wp_footer(); ?>

</body>



</html>