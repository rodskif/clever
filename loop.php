<?php
    $args = array(
        "post_type" => 'post',
        "post_per_page" => 68,
        "orderby" => 'ID'
    );
    $p = new WP_Query($args);
    if ($p->have_posts()) { 
?>
<div class="container">
    <div class="row">

<?php while ($p->have_posts()) {
             $p->the_post();
 ?> 
        <div class="col-md-4 space">
            <?php if (has_post_thumbnail()) { ?>
            <?php the_post_thumbnail(array(400, 300), array('class' => 'img-responsive')) ?>
            <?php } ?>
            <h2><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
            <p><b><?= __('Posted on', 'sg') ?></b> <?php the_date(); ?> <?= __('at', 'sg') ?> <?php the_time(); ?></p>
            <p><b><?= __('Tags', 'sg') ?> </b> <?php the_tags(''); ?> </p>
            <p><b><?= __('Comments', 'sg') ?> </b> <?php $comments_count = wp_count_comments( $post->ID ); echo $comments_count->total_comments ?> </p>
        
            <?php the_excerpt(); ?>

            <a class="btn btn-info btn-lg" href="<?php the_permalink(); ?>"><?= __('Read More', 'sg') ?></a>
        </div>
        <?php } } ?>
        
    </div>
</div>

        