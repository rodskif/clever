/*
 * jQuery FlexSlider v2.2.0
 * Copyright 2012 WooThemes
 * Contributing Author: Tyler Smith
 */
;
(function ($) {

  //FlexSlider: Object Instance
  $.flexslider = function(el, options) {
    var slider = $(el);

    // making variables public
    slider.vars = $.extend({}, $.flexslider.defaults, options);

    var namespace = slider.vars.namespace,
        msGesture = window.navigator && window.navigator.msPointerEnabled && window.MSGesture,
        touch = (( "ontouchstart" in window ) || msGesture || window.DocumentTouch && document instanceof DocumentTouch) && slider.vars.touch,
        // depricating this idea, as devices are being released with both of these events
        //eventType = (touch) ? "touchend" : "click",
        eventType = "click touchend MSPointerUp",
        watchedEvent = "",
        watchedEventClearTimer,
        vertical = slider.vars.direction === "vertical",
        reverse = slider.vars.reverse,
        carousel = (slider.vars.itemWidth > 0),
        fade = slider.vars.animation === "fade",
        asNav = slider.vars.asNavFor !== "",
        methods = {},
        focused = true;

    // Store a reference to the slider object
    $.data(el, "flexslider", slider);

    // Private slider methods
    methods = {
      init: function() {
        slider.animating = false;
        // Get current slide and make sure it is a number
        slider.currentSlide = parseInt( ( slider.vars.startAt ? slider.vars.startAt : 0) );
        if ( isNaN( slider.currentSlide ) ) slider.currentSlide = 0;
        slider.animatingTo = slider.currentSlide;
        slider.atEnd = (slider.currentSlide === 0 || slider.currentSlide === slider.last);
        slider.containerSelector = slider.vars.selector.substr(0,slider.vars.selector.search(' '));
        slider.slides = $(slider.vars.selector, slider);
        slider.container = $(slider.containerSelector, slider);
        slider.count = slider.slides.length;
        // SYNC:
        slider.syncExists = $(slider.vars.sync).length > 0;
        // SLIDE:
        if (slider.vars.animation === "slide") slider.vars.animation = "swing";
        slider.prop = (vertical) ? "top" : "marginLeft";
        slider.args = {};
        // SLIDESHOW:
        slider.manualPause = false;
        slider.stopped = false;
        //PAUSE WHEN INVISIBLE
        slider.started = false;
        slider.startTimeout = null;
        // TOUCH/USECSS:
        slider.transitions = !slider.vars.video && !fade && slider.vars.useCSS && (function() {
          var obj = document.createElement('div'),
              props = ['perspectiveProperty', 'WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
          for (var i in props) {
            if ( obj.style[ props[i] ] !== undefined ) {
              slider.pfx = props[i].replace('Perspective','').toLowerCase();
              slider.prop = "-" + slider.pfx + "-transform";
              return true;
            }
          }
          return false;
        }());
        // CONTROLSCONTAINER:
        if (slider.vars.controlsContainer !== "") slider.controlsContainer = $(slider.vars.controlsContainer).length > 0 && $(slider.vars.controlsContainer);
        // MANUAL:
        if (slider.vars.manualControls !== "") slider.manualControls = $(slider.vars.manualControls).length > 0 && $(slider.vars.manualControls);

        // RANDOMIZE:
        if (slider.vars.randomize) {
          slider.slides.sort(function() { return (Math.round(Math.random())-0.5); });
          slider.container.empty().append(slider.slides);
        }

        slider.doMath();

        // INIT
        slider.setup("init");

        // CONTROLNAV:
        if (slider.vars.controlNav) methods.controlNav.setup();

        // DIRECTIONNAV:
        if (slider.vars.directionNav) methods.directionNav.setup();

        // KEYBOARD:
        if (slider.vars.keyboard && ($(slider.containerSelector).length === 1 || slider.vars.multipleKeyboard)) {
          $(document).bind('keyup', function(event) {
            var keycode = event.keyCode;
            if (!slider.animating && (keycode === 39 || keycode === 37)) {
              var target = (keycode === 39) ? slider.getTarget('next') :
                           (keycode === 37) ? slider.getTarget('prev') : false;
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }
          });
        }
        // MOUSEWHEEL:
        if (slider.vars.mousewheel) {
          slider.bind('mousewheel', function(event, delta, deltaX, deltaY) {
            event.preventDefault();
            var target = (delta < 0) ? slider.getTarget('next') : slider.getTarget('prev');
            slider.flexAnimate(target, slider.vars.pauseOnAction);
          });
        }

        // PAUSEPLAY
        if (slider.vars.pausePlay) methods.pausePlay.setup();

        //PAUSE WHEN INVISIBLE
        if (slider.vars.slideshow && slider.vars.pauseInvisible) methods.pauseInvisible.init();

        // SLIDSESHOW
        if (slider.vars.slideshow) {
          if (slider.vars.pauseOnHover) {
            slider.hover(function() {
              if (!slider.manualPlay && !slider.manualPause) slider.pause();
            }, function() {
              if (!slider.manualPause && !slider.manualPlay && !slider.stopped) slider.play();
            });
          }
          // initialize animation
          //If we're visible, or we don't use PageVisibility API
          if(!slider.vars.pauseInvisible || !methods.pauseInvisible.isHidden()) {
            (slider.vars.initDelay > 0) ? slider.startTimeout = setTimeout(slider.play, slider.vars.initDelay) : slider.play();
          }
        }

        // ASNAV:
        if (asNav) methods.asNav.setup();

        // TOUCH
        if (touch && slider.vars.touch) methods.touch();

        // FADE&&SMOOTHHEIGHT || SLIDE:
        if (!fade || (fade && slider.vars.smoothHeight)) $(window).bind("resize orientationchange focus", methods.resize);

        slider.find("img").attr("draggable", "false");

        // API: start() Callback
        setTimeout(function(){
          slider.vars.start(slider);
        }, 200);
      },
      asNav: {
        setup: function() {
          slider.asNav = true;
          slider.animatingTo = Math.floor(slider.currentSlide/slider.move);
          slider.currentItem = slider.currentSlide;
          slider.slides.removeClass(namespace + "active-slide").eq(slider.currentItem).addClass(namespace + "active-slide");
          if(!msGesture){
              slider.slides.click(function(e){
                e.preventDefault();
                var $slide = $(this),
                    target = $slide.index();
                var posFromLeft = $slide.offset().left - $(slider).scrollLeft(); // Find position of slide relative to left of slider container
                if( posFromLeft <= 0 && $slide.hasClass( namespace + 'active-slide' ) ) {
                  slider.flexAnimate(slider.getTarget("prev"), true);
                } else if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass(namespace + "active-slide")) {
                  slider.direction = (slider.currentItem < target) ? "next" : "prev";
                  slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                }
              });
          }else{
              el._slider = slider;
              slider.slides.each(function (){
                  var that = this;
                  that._gesture = new MSGesture();
                  that._gesture.target = that;
                  that.addEventListener("MSPointerDown", function (e){
                      e.preventDefault();
                      if(e.currentTarget._gesture)
                          e.currentTarget._gesture.addPointer(e.pointerId);
                  }, false);
                  that.addEventListener("MSGestureTap", function (e){
                      e.preventDefault();
                      var $slide = $(this),
                          target = $slide.index();
                      if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass('active')) {
                          slider.direction = (slider.currentItem < target) ? "next" : "prev";
                          slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                      }
                  });
              });
          }
        }
      },
      controlNav: {
        setup: function() {
          if (!slider.manualControls) {
            methods.controlNav.setupPaging();
          } else { // MANUALCONTROLS:
            methods.controlNav.setupManual();
          }
        },
        setupPaging: function() {
          var type = (slider.vars.controlNav === "thumbnails") ? 'control-thumbs' : 'control-paging',
              j = 1,
              item,
              slide;

          slider.controlNavScaffold = $('<ol class="'+ namespace + 'control-nav ' + namespace + type + '"></ol>');

          if (slider.pagingCount > 1) {
            for (var i = 0; i < slider.pagingCount; i++) {
              slide = slider.slides.eq(i);
              item = (slider.vars.controlNav === "thumbnails") ? '<img src="' + slide.attr( 'data-thumb' ) + '"/>' : '<a>' + j + '</a>';
              if ( 'thumbnails' === slider.vars.controlNav && true === slider.vars.thumbCaptions ) {
                var captn = slide.attr( 'data-thumbcaption' );
                if ( '' != captn && undefined != captn ) item += '<span class="' + namespace + 'caption">' + captn + '</span>';
              }
              slider.controlNavScaffold.append('<li>' + item + '</li>');
              j++;
            }
          }

          // CONTROLSCONTAINER: // GDLR EDIT
          (slider.controlsContainer) ? $(slider.controlsContainer).append(slider.controlNavScaffold) : slider.append($('<div />').addClass('flex-control-nav-wrapper').append(slider.controlNavScaffold));
          methods.controlNav.set();

          methods.controlNav.active();

          slider.controlNavScaffold.delegate('a, img', eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                slider.direction = (target > slider.currentSlide) ? "next" : "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();

          });
        },
        setupManual: function() {
          slider.controlNav = slider.manualControls;
          methods.controlNav.active();

          slider.controlNav.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                (target > slider.currentSlide) ? slider.direction = "next" : slider.direction = "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        set: function() {
          var selector = (slider.vars.controlNav === "thumbnails") ? 'img' : 'a';
          slider.controlNav = $('.' + namespace + 'control-nav li ' + selector, (slider.controlsContainer) ? slider.controlsContainer : slider);
        },
        active: function() {
          slider.controlNav.removeClass(namespace + "active").eq(slider.animatingTo).addClass(namespace + "active");
        },
        update: function(action, pos) {
          if (slider.pagingCount > 1 && action === "add") {
            slider.controlNavScaffold.append($('<li><a>' + slider.count + '</a></li>'));
          } else if (slider.pagingCount === 1) {
            slider.controlNavScaffold.find('li').remove();
          } else {
            slider.controlNav.eq(pos).closest('li').remove();
          }
          methods.controlNav.set();
          (slider.pagingCount > 1 && slider.pagingCount !== slider.controlNav.length) ? slider.update(pos, action) : methods.controlNav.active();
        }
      },
      directionNav: {
        setup: function() {
          var directionNavScaffold = $('<ul class="' + namespace + 'direction-nav"><li><a class="' + namespace + 'prev" href="#">' + slider.vars.prevText + '</a></li><li><a class="' + namespace + 'next" href="#">' + slider.vars.nextText + '</a></li></ul>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            $(slider.controlsContainer).append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider.controlsContainer);
          } else {
            slider.append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider);
          }

          methods.directionNav.update();

          slider.directionNav.bind(eventType, function(event) {
            event.preventDefault();
            var target;

            if (watchedEvent === "" || watchedEvent === event.type) {
              target = ($(this).hasClass(namespace + 'next')) ? slider.getTarget('next') : slider.getTarget('prev');
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function() {
          var disabledClass = namespace + 'disabled';
          if (slider.pagingCount === 1) {
            slider.directionNav.addClass(disabledClass).attr('tabindex', '-1');
          } else if (!slider.vars.animationLoop) {
            if (slider.animatingTo === 0) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "prev").addClass(disabledClass).attr('tabindex', '-1');
            } else if (slider.animatingTo === slider.last) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "next").addClass(disabledClass).attr('tabindex', '-1');
            } else {
              slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
            }
          } else {
            slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
          }
        }
      },
      pausePlay: {
        setup: function() {
          var pausePlayScaffold = $('<div class="' + namespace + 'pauseplay"><a></a></div>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            slider.controlsContainer.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider.controlsContainer);
          } else {
            slider.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider);
          }

          methods.pausePlay.update((slider.vars.slideshow) ? namespace + 'pause' : namespace + 'play');

          slider.pausePlay.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              if ($(this).hasClass(namespace + 'pause')) {
                slider.manualPause = true;
                slider.manualPlay = false;
                slider.pause();
              } else {
                slider.manualPause = false;
                slider.manualPlay = true;
                slider.play();
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function(state) {
          (state === "play") ? slider.pausePlay.removeClass(namespace + 'pause').addClass(namespace + 'play').html(slider.vars.playText) : slider.pausePlay.removeClass(namespace + 'play').addClass(namespace + 'pause').html(slider.vars.pauseText);
        }
      },
      touch: function() {
        var startX,
          startY,
          offset,
          cwidth,
          dx,
          startT,
          scrolling = false,
          localX = 0,
          localY = 0,
          accDx = 0;

        if(!msGesture){
            el.addEventListener('touchstart', onTouchStart, false);

            function onTouchStart(e) {
              if (slider.animating) {
                e.preventDefault();
              } else if ( ( window.navigator.msPointerEnabled ) || e.touches.length === 1 ) {
                slider.pause();
                // CAROUSEL:
                cwidth = (vertical) ? slider.h : slider. w;
                startT = Number(new Date());
                // CAROUSEL:

                // Local vars for X and Y points.
                localX = e.touches[0].pageX;
                localY = e.touches[0].pageY;

                offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                         (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                         (carousel && slider.currentSlide === slider.last) ? slider.limit :
                         (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                         (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                startX = (vertical) ? localY : localX;
                startY = (vertical) ? localX : localY;

                el.addEventListener('touchmove', onTouchMove, false);
                el.addEventListener('touchend', onTouchEnd, false);
              }
            }

            function onTouchMove(e) {
              // Local vars for X and Y points.

              localX = e.touches[0].pageX;
              localY = e.touches[0].pageY;

              dx = (vertical) ? startX - localY : startX - localX;
              scrolling = (vertical) ? (Math.abs(dx) < Math.abs(localX - startY)) : (Math.abs(dx) < Math.abs(localY - startY));

              var fxms = 500;

              if ( ! scrolling || Number( new Date() ) - startT > fxms ) {
                e.preventDefault();
                if (!fade && slider.transitions) {
                  if (!slider.vars.animationLoop) {
                    dx = dx/((slider.currentSlide === 0 && dx < 0 || slider.currentSlide === slider.last && dx > 0) ? (Math.abs(dx)/cwidth+2) : 1);
                  }
                  slider.setProps(offset + dx, "setTouch");
                }
              }
            }

            function onTouchEnd(e) {
              // finish the touch by undoing the touch session
              el.removeEventListener('touchmove', onTouchMove, false);

              if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                var updateDx = (reverse) ? -dx : dx,
                    target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                  slider.flexAnimate(target, slider.vars.pauseOnAction);
                } else {
                  if (!fade) slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                }
              }
              el.removeEventListener('touchend', onTouchEnd, false);

              startX = null;
              startY = null;
              dx = null;
              offset = null;
            }
        }else{
            el.style.msTouchAction = "none";
            el._gesture = new MSGesture();
            el._gesture.target = el;
            el.addEventListener("MSPointerDown", onMSPointerDown, false);
            el._slider = slider;
            el.addEventListener("MSGestureChange", onMSGestureChange, false);
            el.addEventListener("MSGestureEnd", onMSGestureEnd, false);

            function onMSPointerDown(e){
                e.stopPropagation();
                if (slider.animating) {
                    e.preventDefault();
                }else{
                    slider.pause();
                    el._gesture.addPointer(e.pointerId);
                    accDx = 0;
                    cwidth = (vertical) ? slider.h : slider. w;
                    startT = Number(new Date());
                    // CAROUSEL:

                    offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                        (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                            (carousel && slider.currentSlide === slider.last) ? slider.limit :
                                (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                                    (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                }
            }

            function onMSGestureChange(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                var transX = -e.translationX,
                    transY = -e.translationY;

                //Accumulate translations.
                accDx = accDx + ((vertical) ? transY : transX);
                dx = accDx;
                scrolling = (vertical) ? (Math.abs(accDx) < Math.abs(-transX)) : (Math.abs(accDx) < Math.abs(-transY));

                if(e.detail === e.MSGESTURE_FLAG_INERTIA){
                    setImmediate(function (){
                        el._gesture.stop();
                    });

                    return;
                }

                if (!scrolling || Number(new Date()) - startT > 500) {
                    e.preventDefault();
                    if (!fade && slider.transitions) {
                        if (!slider.vars.animationLoop) {
                            dx = accDx / ((slider.currentSlide === 0 && accDx < 0 || slider.currentSlide === slider.last && accDx > 0) ? (Math.abs(accDx) / cwidth + 2) : 1);
                        }
                        slider.setProps(offset + dx, "setTouch");
                    }
                }
            }

            function onMSGestureEnd(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                    var updateDx = (reverse) ? -dx : dx,
                        target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                    if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                        slider.flexAnimate(target, slider.vars.pauseOnAction);
                    } else {
                        if (!fade) slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                    }
                }

                startX = null;
                startY = null;
                dx = null;
                offset = null;
                accDx = 0;
            }
        }
      },
      resize: function() {
        if (!slider.animating && slider.is(':visible')) {
          if (!carousel) slider.doMath();

          if (fade) {
            // SMOOTH HEIGHT:
            methods.smoothHeight();
          } else if (carousel) { //CAROUSEL:
            slider.slides.width(slider.computedW);
            slider.update(slider.pagingCount);
            slider.setProps();
          }
          else if (vertical) { //VERTICAL:
            slider.viewport.height(slider.h);
            slider.setProps(slider.h, "setTotal");
          } else {
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) methods.smoothHeight();
            slider.newSlides.width(slider.computedW);
            slider.setProps(slider.computedW, "setTotal");
          }
        }
      },
      smoothHeight: function(dur) {
        if (!vertical || fade) {
          var $obj = (fade) ? slider : slider.viewport;
          (dur) ? $obj.animate({"height": slider.slides.eq(slider.animatingTo).outerHeight()}, dur) : $obj.height(slider.slides.eq(slider.animatingTo).outerHeight());
        }
      },
      sync: function(action) {
        var $obj = $(slider.vars.sync).data("flexslider"),
            target = slider.animatingTo;

        switch (action) {
          case "animate": $obj.flexAnimate(target, slider.vars.pauseOnAction, false, true); break;
          case "play": if (!$obj.playing && !$obj.asNav) { $obj.play(); } break;
          case "pause": $obj.pause(); break;
        }
      },
      pauseInvisible: {
        visProp: null,
        init: function() {
          var prefixes = ['webkit','moz','ms','o'];

          if ('hidden' in document) return 'hidden';
          for (var i = 0; i < prefixes.length; i++) {
            if ((prefixes[i] + 'Hidden') in document) 
            methods.pauseInvisible.visProp = prefixes[i] + 'Hidden';
          }
          if (methods.pauseInvisible.visProp) {
            var evtname = methods.pauseInvisible.visProp.replace(/[H|h]idden/,'') + 'visibilitychange';
            document.addEventListener(evtname, function() {
              if (methods.pauseInvisible.isHidden()) {
                if(slider.startTimeout) clearTimeout(slider.startTimeout); //If clock is ticking, stop timer and prevent from starting while invisible
                else slider.pause(); //Or just pause
              }
              else {
                if(slider.started) slider.play(); //Initiated before, just play
                else (slider.vars.initDelay > 0) ? setTimeout(slider.play, slider.vars.initDelay) : slider.play(); //Didn't init before: simply init or wait for it
              }
            });
          }       
        },
        isHidden: function() {
          return document[methods.pauseInvisible.visProp] || false;
        }
      },
      setToClearWatchedEvent: function() {
        clearTimeout(watchedEventClearTimer);
        watchedEventClearTimer = setTimeout(function() {
          watchedEvent = "";
        }, 3000);
      }
    }

    // public methods
    slider.flexAnimate = function(target, pause, override, withSync, fromNav) {
      if (!slider.vars.animationLoop && target !== slider.currentSlide) {
        slider.direction = (target > slider.currentSlide) ? "next" : "prev";
      }

      if (asNav && slider.pagingCount === 1) slider.direction = (slider.currentItem < target) ? "next" : "prev";

      if (!slider.animating && (slider.canAdvance(target, fromNav) || override) && slider.is(":visible")) {
        if (asNav && withSync) {
          var master = $(slider.vars.asNavFor).data('flexslider');
          slider.atEnd = target === 0 || target === slider.count - 1;
          master.flexAnimate(target, true, false, true, fromNav);
          slider.direction = (slider.currentItem < target) ? "next" : "prev";
          master.direction = slider.direction;

          if (Math.ceil((target + 1)/slider.visible) - 1 !== slider.currentSlide && target !== 0) {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            target = Math.floor(target/slider.visible);
          } else {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            return false;
          }
        }

        slider.animating = true;
        slider.animatingTo = target;

        // SLIDESHOW:
        if (pause) slider.pause();

        // API: before() animation Callback
        slider.vars.before(slider);

        // SYNC:
        if (slider.syncExists && !fromNav) methods.sync("animate");

        // CONTROLNAV
        if (slider.vars.controlNav) methods.controlNav.active();

        // !CAROUSEL:
        // CANDIDATE: slide active class (for add/remove slide)
        if (!carousel) slider.slides.removeClass(namespace + 'active-slide').eq(target).addClass(namespace + 'active-slide');

        // INFINITE LOOP:
        // CANDIDATE: atEnd
        slider.atEnd = target === 0 || target === slider.last;

        // DIRECTIONNAV:
        if (slider.vars.directionNav) methods.directionNav.update();

        if (target === slider.last) {
          // API: end() of cycle Callback
          slider.vars.end(slider);
          // SLIDESHOW && !INFINITE LOOP:
          if (!slider.vars.animationLoop) slider.pause();
        }

        // SLIDE:
        if (!fade) {
          var dimension = (vertical) ? slider.slides.filter(':first').height() : slider.computedW,
              margin, slideString, calcNext;

          // INFINITE LOOP / REVERSE:
          if (carousel) {
            //margin = (slider.vars.itemWidth > slider.w) ? slider.vars.itemMargin * 2 : slider.vars.itemMargin;
            margin = slider.vars.itemMargin;
            calcNext = ((slider.itemW + margin) * slider.move) * slider.animatingTo;
            slideString = (calcNext > slider.limit && slider.visible !== 1) ? slider.limit : calcNext;
          } else if (slider.currentSlide === 0 && target === slider.count - 1 && slider.vars.animationLoop && slider.direction !== "next") {
            slideString = (reverse) ? (slider.count + slider.cloneOffset) * dimension : 0;
          } else if (slider.currentSlide === slider.last && target === 0 && slider.vars.animationLoop && slider.direction !== "prev") {
            slideString = (reverse) ? 0 : (slider.count + 1) * dimension;
          } else {
            slideString = (reverse) ? ((slider.count - 1) - target + slider.cloneOffset) * dimension : (target + slider.cloneOffset) * dimension;
          }
          slider.setProps(slideString, "", slider.vars.animationSpeed);
          if (slider.transitions) {
            if (!slider.vars.animationLoop || !slider.atEnd) {
              slider.animating = false;
              slider.currentSlide = slider.animatingTo;
            }
            slider.container.unbind("webkitTransitionEnd transitionend");
            slider.container.bind("webkitTransitionEnd transitionend", function() {
              slider.wrapup(dimension);
            });
          } else {
            slider.container.animate(slider.args, slider.vars.animationSpeed, slider.vars.easing, function(){
              slider.wrapup(dimension);
            });
          }
        } else { // FADE:
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeOut(slider.vars.animationSpeed, slider.vars.easing);
            //slider.slides.eq(target).fadeIn(slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);

            slider.slides.eq(slider.currentSlide).css({"zIndex": 1}).animate({"opacity": 0}, slider.vars.animationSpeed, slider.vars.easing);
            slider.slides.eq(target).css({"zIndex": 2}).animate({"opacity": 1}, slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);

          } else {
            slider.slides.eq(slider.currentSlide).css({ "opacity": 0, "zIndex": 1 });
            slider.slides.eq(target).css({ "opacity": 1, "zIndex": 2 });
            slider.wrapup(dimension);
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) methods.smoothHeight(slider.vars.animationSpeed);
      }
    }
    slider.wrapup = function(dimension) {
      // SLIDE:
      if (!fade && !carousel) {
        if (slider.currentSlide === 0 && slider.animatingTo === slider.last && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpEnd");
        } else if (slider.currentSlide === slider.last && slider.animatingTo === 0 && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpStart");
        }
      }
      slider.animating = false;
      slider.currentSlide = slider.animatingTo;
      // API: after() animation Callback
      slider.vars.after(slider);
    }

    // SLIDESHOW:
    slider.animateSlides = function() {
      if (!slider.animating && focused ) slider.flexAnimate(slider.getTarget("next"));
    }
    // SLIDESHOW:
    slider.pause = function() {
      clearInterval(slider.animatedSlides);
      slider.animatedSlides = null;
      slider.playing = false;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) methods.pausePlay.update("play");
      // SYNC:
      if (slider.syncExists) methods.sync("pause");
    }
    // SLIDESHOW:
    slider.play = function() {
      if (slider.playing) clearInterval(slider.animatedSlides);
      slider.animatedSlides = slider.animatedSlides || setInterval(slider.animateSlides, slider.vars.slideshowSpeed);
      slider.started = slider.playing = true;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) methods.pausePlay.update("pause");
      // SYNC:
      if (slider.syncExists) methods.sync("play");
    }
    // STOP:
    slider.stop = function () {
      slider.pause();
      slider.stopped = true;
    }
    slider.canAdvance = function(target, fromNav) {
      // ASNAV:
      var last = (asNav) ? slider.pagingCount - 1 : slider.last;
      return (fromNav) ? true :
             (asNav && slider.currentItem === slider.count - 1 && target === 0 && slider.direction === "prev") ? true :
             (asNav && slider.currentItem === 0 && target === slider.pagingCount - 1 && slider.direction !== "next") ? false :
             (target === slider.currentSlide && !asNav) ? false :
             (slider.vars.animationLoop) ? true :
             (slider.atEnd && slider.currentSlide === 0 && target === last && slider.direction !== "next") ? false :
             (slider.atEnd && slider.currentSlide === last && target === 0 && slider.direction === "next") ? false :
             true;
    }
    slider.getTarget = function(dir) {
      slider.direction = dir;
      if (dir === "next") {
        return (slider.currentSlide === slider.last) ? 0 : slider.currentSlide + 1;
      } else {
        return (slider.currentSlide === 0) ? slider.last : slider.currentSlide - 1;
      }
    }

    // SLIDE:
    slider.setProps = function(pos, special, dur) {
      var target = (function() {
        var posCheck = (pos) ? pos : ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo,
            posCalc = (function() {
              if (carousel) {
                return (special === "setTouch") ? pos :
                       (reverse && slider.animatingTo === slider.last) ? 0 :
                       (reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                       (slider.animatingTo === slider.last) ? slider.limit : posCheck;
              } else {
                switch (special) {
                  case "setTotal": return (reverse) ? ((slider.count - 1) - slider.currentSlide + slider.cloneOffset) * pos : (slider.currentSlide + slider.cloneOffset) * pos;
                  case "setTouch": return (reverse) ? pos : pos;
                  case "jumpEnd": return (reverse) ? pos : slider.count * pos;
                  case "jumpStart": return (reverse) ? slider.count * pos : pos;
                  default: return pos;
                }
              }
            }());

            return (posCalc * -1) + "px";
          }());

      if (slider.transitions) {
        target = (vertical) ? "translate3d(0," + target + ",0)" : "translate3d(" + target + ",0,0)";
        dur = (dur !== undefined) ? (dur/1000) + "s" : "0s";
        slider.container.css("-" + slider.pfx + "-transition-duration", dur);
      }

      slider.args[slider.prop] = target;
      if (slider.transitions || dur === undefined) slider.container.css(slider.args);
    }

    slider.setup = function(type) {
      // SLIDE:
      if (!fade) {
        var sliderOffset, arr;

        if (type === "init") {
          slider.viewport = $('<div class="' + namespace + 'viewport"></div>').css({"overflow": "hidden", "position": "relative"}).appendTo(slider).append(slider.container);
          // INFINITE LOOP:
          slider.cloneCount = 0;
          slider.cloneOffset = 0;
          // REVERSE:
          if (reverse) {
            arr = $.makeArray(slider.slides).reverse();
            slider.slides = $(arr);
            slider.container.empty().append(slider.slides);
          }
        }
        // INFINITE LOOP && !CAROUSEL:
        if (slider.vars.animationLoop && !carousel) {
          slider.cloneCount = 2;
          slider.cloneOffset = 1;
          // clear out old clones
          if (type !== "init") slider.container.find('.clone').remove();
          slider.container.append(slider.slides.first().clone().addClass('clone').attr('aria-hidden', 'true')).prepend(slider.slides.last().clone().addClass('clone').attr('aria-hidden', 'true'));
        }
        slider.newSlides = $(slider.vars.selector, slider);

        sliderOffset = (reverse) ? slider.count - 1 - slider.currentSlide + slider.cloneOffset : slider.currentSlide + slider.cloneOffset;
        // VERTICAL:
        if (vertical && !carousel) {
          slider.container.height((slider.count + slider.cloneCount) * 200 + "%").css("position", "absolute").width("100%");
          setTimeout(function(){
            slider.newSlides.css({"display": "block"});
            slider.doMath();
            slider.viewport.height(slider.h);
            slider.setProps(sliderOffset * slider.h, "init");
          }, (type === "init") ? 100 : 0);
        } else {
          slider.container.width((slider.count + slider.cloneCount) * 200 + "%");
          slider.setProps(sliderOffset * slider.computedW, "init");
          setTimeout(function(){
            slider.doMath();
            slider.newSlides.css({"width": slider.computedW, "float": "left", "display": "block"});
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) methods.smoothHeight();
          }, (type === "init") ? 100 : 0);
        }
      } else { // FADE:
        slider.slides.css({"width": "100%", "float": "left", "marginRight": "-100%", "position": "relative"});
        if (type === "init") {
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeIn(slider.vars.animationSpeed, slider.vars.easing);
            slider.slides.css({ "opacity": 0, "display": "block", "zIndex": 1 }).eq(slider.currentSlide).css({"zIndex": 2}).animate({"opacity": 1},slider.vars.animationSpeed,slider.vars.easing);
          } else {
            slider.slides.css({ "opacity": 0, "display": "block", "webkitTransition": "opacity " + slider.vars.animationSpeed / 1000 + "s ease", "zIndex": 1 }).eq(slider.currentSlide).css({ "opacity": 1, "zIndex": 2});
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) methods.smoothHeight();
      }
      // !CAROUSEL:
      // CANDIDATE: active slide
      if (!carousel) slider.slides.removeClass(namespace + "active-slide").eq(slider.currentSlide).addClass(namespace + "active-slide");
    }


    slider.doMath = function() {
      var slide = slider.slides.first(),
          slideMargin = slider.vars.itemMargin,
          minItems = slider.vars.minItems,
          maxItems = slider.vars.maxItems;
		  
		  // gdlr modify
		  if( $(window).width() < 767 ){ minItems = 1; maxItems = 1; }
		  if( $(window).width() < 419 ){ minItems = 1; maxItems = 1; }

      slider.w = (slider.viewport===undefined) ? slider.width() : slider.viewport.width();
      slider.h = slide.height();
      slider.boxPadding = slide.outerWidth() - slide.width();

      // CAROUSEL:
      if (carousel) {
        slider.itemT = slider.vars.itemWidth + slideMargin;
        slider.minW = (minItems) ? minItems * slider.itemT : slider.w;
        slider.maxW = (maxItems) ? (maxItems * slider.itemT) - slideMargin : slider.w;
        slider.itemW = (slider.minW > slider.w) ? (slider.w - (slideMargin * (minItems - 1)))/minItems :
                       (slider.maxW < slider.w) ? (slider.w - (slideMargin * (maxItems - 1)))/maxItems :
                       (slider.vars.itemWidth > slider.w) ? slider.w : slider.vars.itemWidth;

        slider.visible = Math.floor((slider.w+slideMargin)/(slider.itemW+slideMargin-1));
        slider.move = (slider.vars.move > 0 && slider.vars.move < slider.visible ) ? slider.vars.move : slider.visible;
        slider.pagingCount = Math.ceil(((slider.count - slider.visible)/slider.move) + 1);
        slider.last =  slider.pagingCount - 1;
        slider.limit = (slider.pagingCount === 1) ? 0 :
                       (slider.vars.itemWidth > slider.w) ? (slider.itemW * (slider.count - 1)) + (slideMargin * (slider.count - 1)) : ((slider.itemW + slideMargin) * slider.count) - slider.w - slideMargin;
      } else {
        slider.itemW = slider.w;
        slider.pagingCount = slider.count;
        slider.last = slider.count - 1;
      }
      slider.computedW = slider.itemW - slider.boxPadding;
    }


    slider.update = function(pos, action) {
      slider.doMath();

      // update currentSlide and slider.animatingTo if necessary
      if (!carousel) {
        if (pos < slider.currentSlide) {
          slider.currentSlide += 1;
        } else if (pos <= slider.currentSlide && pos !== 0) {
          slider.currentSlide -= 1;
        }
        slider.animatingTo = slider.currentSlide;
      }

      // update controlNav
      if (slider.vars.controlNav && !slider.manualControls) {
        if ((action === "add" && !carousel) || slider.pagingCount > slider.controlNav.length) {
          methods.controlNav.update("add");
        } else if ((action === "remove" && !carousel) || slider.pagingCount < slider.controlNav.length) {
          if (carousel && slider.currentSlide > slider.last) {
            slider.currentSlide -= 1;
            slider.animatingTo -= 1;
          }
          methods.controlNav.update("remove", slider.last);
        }
      }
      // update directionNav
      if (slider.vars.directionNav) methods.directionNav.update();

    }

    slider.addSlide = function(obj, pos) {
      var $obj = $(obj);

      slider.count += 1;
      slider.last = slider.count - 1;

      // append new slide
      if (vertical && reverse) {
        (pos !== undefined) ? slider.slides.eq(slider.count - pos).after($obj) : slider.container.prepend($obj);
      } else {
        (pos !== undefined) ? slider.slides.eq(pos).before($obj) : slider.container.append($obj);
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.update(pos, "add");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      //FlexSlider: added() Callback
      slider.vars.added(slider);
    }
    slider.removeSlide = function(obj) {
      var pos = (isNaN(obj)) ? slider.slides.index($(obj)) : obj;

      // update count
      slider.count -= 1;
      slider.last = slider.count - 1;

      // remove slide
      if (isNaN(obj)) {
        $(obj, slider.slides).remove();
      } else {
        (vertical && reverse) ? slider.slides.eq(slider.last).remove() : slider.slides.eq(obj).remove();
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.doMath();
      slider.update(pos, "remove");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      // FlexSlider: removed() Callback
      slider.vars.removed(slider);
    }

    //FlexSlider: Initialize
    methods.init();
  }

  // Ensure the slider isn't focussed if the window loses focus.
  $( window ).blur( function ( e ) {
    focused = false;
  }).focus( function ( e ) {
    focused = true;
  });

  //FlexSlider: Default Settings
  $.flexslider.defaults = {
    namespace: "flex-",             //{NEW} String: Prefix string attached to the class of every element generated by the plugin
    selector: ".slides:first > li",       //{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
    animation: "fade",              //String: Select your animation type, "fade" or "slide"
    easing: "swing",                //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
    direction: "horizontal",        //String: Select the sliding direction, "horizontal" or "vertical"
    reverse: false,                 //{NEW} Boolean: Reverse the animation direction
    animationLoop: true,            //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
    smoothHeight: false,            //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode
    startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
    slideshow: true,                //Boolean: Animate slider automatically
    slideshowSpeed: 7000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 600,            //Integer: Set the speed of animations, in milliseconds
    initDelay: 0,                   //{NEW} Integer: Set an initialization delay, in milliseconds
    randomize: false,               //Boolean: Randomize slide order
    thumbCaptions: false,           //Boolean: Whether or not to put captions on thumbnails when using the "thumbnails" controlNav.

    // Usability features
    pauseOnAction: true,            //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
    pauseOnHover: false,            //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
    pauseInvisible: true,   		//{NEW} Boolean: Pause the slideshow when tab is invisible, resume when visible. Provides better UX, lower CPU usage.
    useCSS: true,                   //{NEW} Boolean: Slider will use CSS3 transitions if available
    touch: true,                    //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
    video: false,                   //{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches

    // Primary Controls
    controlNav: true,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
    directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
    prevText: "Previous",           //String: Set the text for the "previous" directionNav item
    nextText: "Next",               //String: Set the text for the "next" directionNav item

    // Secondary Navigation
    keyboard: true,                 //Boolean: Allow slider navigating via keyboard left/right keys
    multipleKeyboard: false,        //{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
    mousewheel: false,              //{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
    pausePlay: false,               //Boolean: Create pause/play dynamic element
    pauseText: "Pause",             //String: Set the text for the "pause" pausePlay item
    playText: "Play",               //String: Set the text for the "play" pausePlay item

    // Special properties
    controlsContainer: "",          //{UPDATED} jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be $(".flexslider-container"). Property is ignored if given element is not found.
    manualControls: "",             //{UPDATED} jQuery Object/Selector: Declare custom control navigation. Examples would be $(".flex-control-nav li") or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
    sync: "",                       //{NEW} Selector: Mirror the actions performed on this slider with another slider. Use with care.
    asNavFor: "",                   //{NEW} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider

    // Carousel Options
    itemWidth: 0,                   //{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
    itemMargin: 0,                  //{NEW} Integer: Margin between carousel items.
    minItems: 1,                    //{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
    maxItems: 0,                    //{NEW} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
    move: 0,                        //{NEW} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.
    allowOneSlide: true,           //{NEW} Boolean: Whether or not to allow a slider comprised of a single slide

    // Callback API
    start: function(){},            //Callback: function(slider) - Fires when the slider loads the first slide
    before: function(){},           //Callback: function(slider) - Fires asynchronously with each slider animation
    after: function(){},            //Callback: function(slider) - Fires after each slider animation completes
    end: function(){},              //Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
    added: function(){},            //{NEW} Callback: function(slider) - Fires after a slide is added
    removed: function(){}           //{NEW} Callback: function(slider) - Fires after a slide is removed
  }


  //FlexSlider: Plugin Function
  $.fn.flexslider = function(options) {
    if (options === undefined) options = {};

    if (typeof options === "object") {
      return this.each(function() {
        var $this = $(this),
            selector = (options.selector) ? options.selector : ".slides > li",
            $slides = $this.find(selector);

      if ( ( $slides.length === 1 && options.allowOneSlide === true ) || $slides.length === 0 ) {
          $slides.fadeIn(400);
          if (options.start) options.start($this);
        } else if ($this.data('flexslider') === undefined) {
          new $.flexslider(this, options);
        }
      });
    } else {
      // Helper strings to quickly perform functions on the slider
      var $slider = $(this).data('flexslider');
      switch (options) {
        case "play": $slider.play(); break;
        case "pause": $slider.pause(); break;
        case "stop": $slider.stop(); break;
        case "next": $slider.flexAnimate($slider.getTarget("next"), true); break;
        case "prev":
        case "previous": $slider.flexAnimate($slider.getTarget("prev"), true); break;
        default: if (typeof options === "number") $slider.flexAnimate(options, true);
      }
    }
  }
})(jQuery);

(function($){
    
  if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || 
    navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || 
    navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || 
    navigator.userAgent.match(/Windows Phone/i) ){ 
    var gdlr_touch_device = true; 
  }else{ 
    var gdlr_touch_device = false; 
  }
  
  // retrieve GET variable from url
  $.extend({
    getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
    },
    getUrlVar: function(name){
    return $.getUrlVars()[name];
    }
  }); 
  
  // blog - port nav
  function gdlr_set_item_outer_nav(){
    $('.blog-item-wrapper > .gdlr-nav-container').each(function(){
      var container = $(this).siblings('.blog-item-holder');
      var child = $(this).children();
      child.css({ 'top':container.position().top, 'bottom':'auto', height: container.height() - 50 });
    });
    $('.portfolio-item-wrapper > .gdlr-nav-container').each(function(){
      var container = $(this).siblings('.portfolio-item-holder');
      var child = $(this).children();
      child.css({ 'top':container.position().top, 'bottom':'auto', height: container.height() - 40 });
    });   
  } 
  
  // runs flex slider function
  $.fn.gdlr_flexslider = function(){
    if(typeof($.fn.flexslider) == 'function'){
      $(this).each(function(){
        var flex_attr = {
          animation: 'fade',
          animationLoop: true,
          prevText: '<i class="icon-angle-left" ></i>', 
          nextText: '<i class="icon-angle-right" ></i>',
          useCSS: false
        };
        
        // slide duration
        if( $(this).attr('data-pausetime') ){
          flex_attr.slideshowSpeed = parseInt($(this).attr('data-pausetime'));
        }
        if( $(this).attr('data-slidespeed') ){
          flex_attr.animationSpeed = parseInt($(this).attr('data-slidespeed'));
        }

        // set the attribute for carousel type
        if( $(this).attr('data-type') == 'carousel' ){
          flex_attr.move = 1;
          flex_attr.animation = 'slide';
          
          if( $(this).closest('.gdlr-item-no-space').length > 0 ){
            flex_attr.itemWidth = $(this).width() / parseInt($(this).attr('data-columns'));
            flex_attr.itemMargin = 0;             
          }else{
            flex_attr.itemWidth = (($(this).width() + 30) / parseInt($(this).attr('data-columns'))) - 30;
            flex_attr.itemMargin = 30;  
          }   
          
          // if( $(this).attr('data-columns') == "1" ){ flex_attr.smoothHeight = true; }
        }else{
          if( $(this).attr('data-effect') ){
            flex_attr.animation = $(this).attr('data-effect');
          }
        }
        if( $(this).attr('data-columns') ){
          flex_attr.minItems = parseInt($(this).attr('data-columns'));
          flex_attr.maxItems = parseInt($(this).attr('data-columns'));  
        }       
        
        // set the navigation to different area
        if( $(this).attr('data-nav-container') ){
          var flex_parent = $(this).parents('.' + $(this).attr('data-nav-container')).prev('.gdlr-nav-container');
          
          if( flex_parent.find('.gdlr-flex-prev').length > 0 || flex_parent.find('.gdlr-flex-next').length > 0 ){
            flex_attr.controlNav = false;
            flex_attr.directionNav = false;
            flex_attr.start = function(slider){
              flex_parent.find('.gdlr-flex-next').click(function(){
                slider.flexAnimate(slider.getTarget("next"), true);
              });
              flex_parent.find('.gdlr-flex-prev').click(function(){
                slider.flexAnimate(slider.getTarget("prev"), true);
              });
              
              gdlr_set_item_outer_nav();
              $(window).resize(function(){ gdlr_set_item_outer_nav(); });
            }
          }else{
            flex_attr.controlNav = false;
            flex_attr.controlsContainer = flex_parent.find('.nav-container'); 
          }
          
        }

        $(this).flexslider(flex_attr);  
      }); 
    }
  }
  
  // runs nivo slider function
  $.fn.gdlr_nivoslider = function(){
    if(typeof($.fn.nivoSlider) == 'function'){
      $(this).each(function(){
        var nivo_attr = {};
        
        if( $(this).attr('data-pausetime') ){
          nivo_attr.pauseTime = parseInt($(this).attr('data-pausetime'));
        }
        if( $(this).attr('data-slidespeed') ){
          nivo_attr.animSpeed = parseInt($(this).attr('data-slidespeed'));
        }
        if( $(this).attr('data-effect') ){
          nivo_attr.effect = $(this).attr('data-effect');
        }

        $(this).nivoSlider(nivo_attr);  
      }); 
    }
  } 
  
  // runs isotope function
  $.fn.gdlr_isotope = function(){
    if(typeof($.fn.isotope) == 'function'){
      $(this).each(function(){
        var layout = ($(this).attr('data-layout'))? $(this).attr('data-layout'): 'fitRows';
        if( layout == 'fitRows' ) return;
        
        // execute isotope
        var isotope_element = $(this);
        isotope_element.children('.clear').remove();
        isotope_element.isotope({
          layoutMode: layout
        });
        
        // resize event
        $(window).resize(function(){
          isotope_element.isotope();
        });       
      }); 
    }
  }
  
  // runs fancybox function
  $.fn.gdlr_fancybox = function(){
    if(typeof($.fn.fancybox) == 'function'){
      var fancybox_attr = {
        nextMethod : 'resizeIn',
        nextSpeed : 250,
        prevMethod : false,
        prevSpeed : 250,  
        helpers : { media : {} }
      };
      
      if( typeof($.fancybox.helpers.thumbs) == 'object' ){
        fancybox_attr.helpers.thumbs = { width: 50, height: 50 };
      }

      $(this).fancybox(fancybox_attr);
    } 
  }
  
  // responsive video
  $.fn.gdlr_fluid_video = function(){
    $(this).find('iframe[src^="http://www.youtube.com"], iframe[src^="//www.youtube.com"],'  +
           'iframe[src^="http://player.vimeo.com"], iframe[src^="//player.vimeo.com"]').each(function(){

      if( ($(this).is('embed') && $(this).parent('object').length) || $(this).parent('.fluid-width-video-wrapper').length ){ return; } 
      if( !$(this).attr('id') ){ $(this).attr('id', 'gdlr-video-' + Math.floor(Math.random()*999999)); }
           
      // ignore if inside layerslider
      if( $(this).closest('.ls-container').length <= 0 ){ 
        var ratio = $(this).height() / $(this).width();
        $(this).removeAttr('height').removeAttr('width');
        $(this).wrap('<div class="gdlr-fluid-video-wrapper"></div>').parent().css('padding-top', (ratio * 100)+"%");
      }
    
    }); 
  }
  
  // pie chart
  $.fn.gdlr_pie_chart = function(){
    if(typeof($.fn.easyPieChart) == 'function'){
      $(this).each(function(){
        var gdlr_chart = $(this);
        
        $(this).easyPieChart({
          animate: 1200,
          lineWidth: gdlr_chart.attr('data-linewidth')? parseInt(gdlr_chart.attr('data-linewidth')): 8,
          size: gdlr_chart.attr('data-size')? parseInt(gdlr_chart.attr('data-size')): 155,
          barColor: gdlr_chart.attr('data-color')? gdlr_chart.attr('data-color'): '#a9e16e',
          trackColor: gdlr_chart.attr('data-bg-color')? gdlr_chart.attr('data-bg-color'): '#f2f2f2',
          backgroundColor: gdlr_chart.attr('data-background'),
          scaleColor: false,
          lineCap: 'square'
        });

        // for responsive purpose
        if($.browser.msie && (parseInt($.browser.version) <= 8)) return;
        function limit_gdlr_chart_size(){
          if( gdlr_chart.parent().width() < parseInt(gdlr_chart.attr('data-size')) ){
            var max_width = gdlr_chart.parent().width() + 'px';
            gdlr_chart.css({'max-width': max_width, 'max-height': max_width});
          }       
        }
        limit_gdlr_chart_size();
        $(window).resize(function(){ limit_gdlr_chart_size(); });
      });
    }
  }
    
  $(document).ready(function(){
  
    // top woocommerce button
    $('.gdlr-top-woocommerce-wrapper').hover(function(){
      $(this).children('.gdlr-top-woocommerce').fadeIn(200);
    }, function(){
      $(this).children('.gdlr-top-woocommerce').fadeOut(200);
    });
  
    // script for accordion item
    $('.gdlr-accordion-item').each(function(){
      var multiple_tab = $(this).hasClass('gdlr-multiple-tab');
      $(this).children('.accordion-tab').children('.accordion-title').click(function(){
        var current_tab = $(this).parent();
        if( current_tab.hasClass('active') ){
          current_tab.removeClass('pre-active');
          $(this).children('i').removeClass('icon-minus').addClass('icon-plus');
          $(this).siblings('.accordion-content').slideUp(function(){ current_tab.removeClass('active'); });
        }else{
          current_tab.addClass('pre-active');
          $(this).children('i').removeClass('icon-plus').addClass('icon-minus');  
          $(this).siblings('.accordion-content').slideDown(function(){ current_tab.addClass('active'); });
                
        }
        
        // close another tab if multiple tab is not allowed ( accordion )
        if( !multiple_tab ){
          current_tab.siblings().removeClass('pre-active');
          current_tab.siblings().children('.accordion-title').children('i').removeClass('icon-minus').addClass('icon-plus');
          current_tab.siblings().children('.accordion-content').slideUp(function(){ $(this).parent().removeClass('active'); });
        }
      });
    });
    
    // script for tab item
    $('.tab-title-wrapper').children().click(function(){
      $(this).addClass('active');
      $(this).siblings().removeClass('active');
      
      var selected_index = $(this).index() + 1;
      $(this).parent().siblings('.tab-content-wrapper').children(':nth-child(' + selected_index + ')').each(function(){
        $(this).siblings().removeClass('active').hide();
        $(this).fadeIn(function(){ $(this).addClass('active'); });
      })
    });   
  
    // initiate the tab when the get tab variable is sent
    var inital_tab = $.getUrlVar('tab');
    if( inital_tab ){ $('#' + inital_tab.replace(',', ', #')).each(function(){ $(this).trigger('click'); }); }
    
    // script for code item
    $('.gdlr-code-item .gdlr-code-title').click(function(){
      var parent = $(this).parent();
      if( parent.hasClass('active') ){
        $(this).children('i').removeClass('icon-minus').addClass('icon-plus');
        $(this).siblings('.gdlr-code-content').slideUp(function(){
          parent.removeClass('active');
        }); 
      }else{
        $(this).children('i').removeClass('icon-plus').addClass('icon-minus');
        $(this).siblings('.gdlr-code-content').slideDown(function(){
          parent.addClass('active');  
        });       
      }
    });   
    
    // script for parallax background
    $('.gdlr-parallax-wrapper').each(function(){
      if( $(this).hasClass('gdlr-background-image') ){
        var parallax_section = $(this);
        var parallax_speed = parseFloat(parallax_section.attr('data-bgspeed'));
        if( parallax_speed == 0 || gdlr_touch_device ) return;
        if( parallax_speed == -1 ){
          parallax_section.css('background-attachment', 'fixed');
          parallax_section.css('background-position', 'center center');
          return;
        }
          
        $(window).scroll(function(){
          // if in area of interest
          if( ( $(window).scrollTop() + $(window).height() > parallax_section.offset().top ) &&
            ( $(window).scrollTop() < parallax_section.offset().top + parallax_section.outerHeight() ) ){
            
            var scroll_pos = 0;
            if( $(window).height() > parallax_section.offset().top ){
              scroll_pos = $(window).scrollTop();
            }else{
              scroll_pos = $(window).scrollTop() + $(window).height() - parallax_section.offset().top;
            }
            parallax_section.css('background-position', 'center ' + (-scroll_pos * parallax_speed) + 'px');
          }
        });     
      }else if( $(this).hasClass('gdlr-background-video') ){
        if(typeof($.fn.mb_YTPlayer) == 'function'){
          $(this).children('.gdlr-bg-player').mb_YTPlayer();
        }
      }else{
        return;
      }
    });
    
    // video responsive
    $('body').gdlr_fluid_video();   
    
    // runs superfish menu
    if(typeof($.fn.superfish) == 'function'){
      
      // create the mega menu script
      $('#gdlr-main-navigation .sf-mega > ul').each(function(){ 
        $(this).children('li').each(function(){
          var current_item = $(this);
          current_item.replaceWith(
            $('<div />').addClass('sf-mega-section')
                  .addClass(current_item.attr('data-column'))
                  .attr('data-size', current_item.attr('data-size'))
                  .html(  $('<div />').addClass('sf-mega-section-inner')
                            .addClass(current_item.attr('class'))
                            .attr('id', current_item.attr('id'))
                            .html(current_item.html())
                  )   
          );
        });
        $(this).replaceWith(this.innerHTML);
      });
      
      // make every menu same height
      $('#gdlr-main-navigation .sf-mega').each(function(){
        var sf_mega = $(this); $(this).show();
        
        var row = 0; var column = 0; var max_height = 0;
        sf_mega.children('.sf-mega-section').each(function(){
          if( column % 60 == 0 ){ 
            if( row != 0 ){
              sf_mega.children('[data-row="' + row + '"]').children('.sf-mega-section-inner').height( max_height - 50 );
              max_height = 0;
            }
            row++; $(this).addClass('first-column'); 
          }   
          
          $(this).attr('data-row', row);  
          column += eval('60*' + $(this).attr('data-size'));
        
          if( $(this).height() > max_height ){
            max_height = $(this).height();
          }
        });
        
        sf_mega.children('[data-row="' + row + '"]').children('.sf-mega-section-inner').height( max_height - 50 );    
      });
      
      $('#gdlr-main-navigation').superfish({
        delay: 100, 
        speed: 'fast', 
        animation: {opacity:'show', height:'show'}
      });   
    }
    
    // responsive menu
    if(typeof($.fn.dlmenu) == 'function'){
      $('#gdlr-responsive-navigation').each(function(){
        $(this).find('.dl-submenu').each(function(){
          if( $(this).siblings('a').attr('href') && $(this).siblings('a').attr('href') != '#' ){
            var parent_nav = $('<li class="menu-item gdlr-parent-menu"></li>');
            parent_nav.append($(this).siblings('a').clone());
            
            $(this).prepend(parent_nav);
          }
        });
        $(this).dlmenu();
      });
    } 
    
    // gallery thumbnail type
    $('.gdlr-gallery-thumbnail').each(function(){
      var thumbnail_container = $(this).children('.gdlr-gallery-thumbnail-container');
    
      $(this).find('.gallery-item').click(function(){
        var selected_slide = thumbnail_container.children('[data-id="' + $(this).attr('data-id') + '"]');
        if(selected_slide.css('display') == 'block') return false;
      
        // check the gallery height
        var image_width = selected_slide.children('img').attr('width');
        var image_ratio = selected_slide.children('img').attr('height') / image_width;
        var temp_height = image_ratio * Math.min(thumbnail_container.width(), image_width);
        
        thumbnail_container.animate({'height': temp_height});
        selected_slide.fadeIn().siblings().hide();
        return false;
      });   

      $(window).resize(function(){ thumbnail_container.css('height', 'auto') });
    });

    // fancybox
    $('a[href$=".jpg"], a[href$=".png"], a[href$=".gif"]').not('[data-rel="fancybox"]').attr('data-rel', 'fancybox');
    $('[data-rel="fancybox"]').gdlr_fancybox();
    
    // image shortcode 
    $('.gdlr-image-link-shortcode').hover(function(){
      $(this).find('.gdlr-image-link-overlay').animate({opacity: 0.8}, 150);
      $(this).find('.gdlr-image-link-icon').animate({opacity: 1}, 150);
    }, function(){
      $(this).find('.gdlr-image-link-overlay').animate({opacity: 0}, 150);
      $(this).find('.gdlr-image-link-icon').animate({opacity: 0}, 150);
    }); 
    
    // Personnel
    $('.gdlr-personnel-item.round-style .personnel-item').each(function(){
      var current_item = $(this);
      function gdlr_set_round_personnel_height(){
        current_item.find('.personnel-item-inner').each(function(){
          $(this).css('margin-top', -($(this).height()/2));
        });
      }
      
      gdlr_set_round_personnel_height();
      $(window).resize(function(){
        gdlr_set_round_personnel_height();
      });
    });
    $('.gdlr-personnel-item.round-style .personnel-item').hover(function(){
      $(this).find('.personnel-author-image').animate({'opacity':0.05}, 200);
      $(this).find('.personnel-item-inner').animate({'opacity':1}, 200);
    }, function(){
      $(this).find('.personnel-author-image').animate({'opacity':1}, 200);
      $(this).find('.personnel-item-inner').animate({'opacity':0}, 200);
    });
    
    // Price Table
    $('.gdlr-price-table-item').each(function(){
      var price_table = $(this);
      
      function set_price_table_height(){
        var max_height = 0;
        var price_content = price_table.find('.price-content');
        
        price_content.css('height', 'auto');
        price_content.each(function(){
          if( max_height < $(this).height() ){ max_height = $(this).height(); }
        });
        price_content.css('height', max_height);
      }
      
      set_price_table_height()
      $(window).resize(function(){ set_price_table_height(); });
    });

    // Default text
    $('form').submit(function(){
      var has_default = false;
      $(this).find('input[data-default]').each(function(){
        if( $(this).is('#url') ){
          if( $(this).val() == $(this).attr('data-default') ) $(this).val('');
        }else{
          if( $(this).val() == $(this).attr('data-default') ) has_default = true;
        }
      });
      
      if(has_default) return false;
    }); 
    
    // Search option
    $('#gdlr-nav-search-form-button').click(function(){
      $('#gdlr-nav-search-form').slideToggle(200);
      return false;
    });
    $('#gdlr-nav-search-form').click(function(event){
      if(event.stopPropagation){
        event.stopPropagation();
      }else if(window.event){
        window.event.cancelBubble=true;
      }
    });
    $("html").click(function(){
      $('#gdlr-nav-search-form').slideUp(200);
    });     
    $('.search-text input[data-default], .gdlr-comments-area input[data-default]').each(function(){
      var default_value = $(this).attr("data-default");
      $(this).val(default_value);
      $(this).live("blur", function(){
        if ($(this).val() == ""){
          $(this).val(default_value);
        } 
      }).live("focus", function(){
        if ($(this).val() == default_value){
          $(this).val("");
        }
      });   
    });   

    // smooth anchor
    if( window.location.hash ){
      $('html, body').animate({
        scrollTop: $(window.location.hash).offset().top - 68
      }, 500);
    }
    $('.gdlr-navigation a[href^="#"], .gdlr-responsive-navigation a[href^="#"]').click(function(){
      if( $(this).attr('href').length > 1 ){
        var item_id = $($(this).attr('href'));
        
        if( $('body').hasClass('home') ){
          if( item_id.length > 0 ){
            $('html, body').animate({
              scrollTop: item_id.offset().top - 68
            }, 500);
            return false;
          }
        }else{
          window.location.replace($('.body-wrapper').attr('data-home') + '/' + $(this).attr('href'));
        }
      }
    }); 
    
    // animate ux
    if( !gdlr_touch_device && ( !$.browser.msie || (parseInt($.browser.version) > 8)) ){
    
      // image ux
      $('.content-wrapper img').each(function(){
        if( $(this).closest('.gdlr-ux, .ls-wp-container, .product, .flexslider, .nivoSlider').length ) return;
        
        var ux_item = $(this);
        if( ux_item.offset().top > $(window).scrollTop() + $(window).height() ){
          ux_item.css({ 'opacity':0 });
        }else{ return; }
        
        $(window).scroll(function(){
          if( $(window).scrollTop() + $(window).height() > ux_item.offset().top + 100 ){
            ux_item.animate({ 'opacity':1 }, 1200); 
          }
        });         
      });
    
      // item ux
      $('.gdlr-ux').each(function(){
        var ux_item = $(this);
        if( ux_item.hasClass('gdlr-chart') || ux_item.hasClass('gdlr-skill-bar') ){
          if( ux_item.offset().top < $(window).scrollTop() + $(window).height() ){
            if( ux_item.hasClass('gdlr-chart') && (!$.browser.msie || (parseInt($.browser.version) > 8)) ){
              ux_item.gdlr_pie_chart();
            }else if( ux_item.hasClass('gdlr-skill-bar') ){
              ux_item.children('.gdlr-skill-bar-progress').each(function(){
                if($(this).attr('data-percent')){
                  $(this).animate({width: $(this).attr('data-percent') + '%'}, 1200, 'easeOutQuart');
                }
              }); 
            }
            return;
          }
        }else if( ux_item.offset().top > $(window).scrollTop() + $(window).height() ){
          ux_item.css({ 'opacity':0, 'padding-top':20, 'margin-bottom':-20 });
        }else{ return; }  

        $(window).scroll(function(){
          if( $(window).scrollTop() + $(window).height() > ux_item.offset().top + 100 ){
            if( ux_item.hasClass('gdlr-chart') && (!$.browser.msie || (parseInt($.browser.version) > 8)) ){
              ux_item.gdlr_pie_chart();
            }else if( ux_item.hasClass('gdlr-skill-bar') ){
              ux_item.children('.gdlr-skill-bar-progress').each(function(){
                if($(this).attr('data-percent')){
                  $(this).animate({width: $(this).attr('data-percent') + '%'}, 1200, 'easeOutQuart');
                }
              }); 
            }else{
              ux_item.animate({ 'opacity':1, 'padding-top':0, 'margin-bottom':0 }, 1200);
            }
          }
        });         
      });
      
    // do not animate on scroll in mobile
    }else{
    
      // Pie chart
      if(!$.browser.msie || (parseInt($.browser.version) > 8)){
        $('.gdlr-chart').gdlr_pie_chart();
      } 

    
      // skill bar
      $('.gdlr-skill-bar-progress').each(function(){
        if($(this).attr('data-percent')){
          $(this).animate({width: $(this).attr('data-percent') + '%'}, 1200, 'easeOutQuart');
        }
      });     
    }   

    // runs nivoslider when available
    $('.nivoSlider').gdlr_nivoslider();   
    
    // runs flexslider when available
    $('.flexslider').gdlr_flexslider();
    
  });
  
  $(window).load(function(){

    // run isotope when available
    $('.gdlr-isotope').gdlr_isotope();  
    
    // run pie chart for ie8 and below
    if($.browser.msie && (parseInt($.browser.version) <= 8)){
      $('.gdlr-chart').gdlr_pie_chart();
    } 

    // float menu
    $('.body-wrapper.float-menu').each(function(){
      var sub_area = $('#gdlr-header-substitute');
      var main_area = sub_area.siblings('.gdlr-header-wrapper');
      
      var logo = main_area.find('.gdlr-logo');
      var logo_img = main_area.find('.gdlr-logo img');
      var navigation = main_area.find('.gdlr-navigation-wrapper');        
      var original = {
        logo_top: logo.css('margin-top'), 
        logo_bottom: logo.css('margin-bottom'), 
        nav_top: navigation.css('margin-top')
      };
        
      $(window).scroll(function(){
        if( main_area.hasClass('gdlr-fixed-header') && ($(this).scrollTop() == 0 || $(this).width() < 959)){
          main_area.children('.top-navigation-wrapper').slideDown(100);

          logo.animate({'margin-top': original.logo_top, 'margin-bottom': original.logo_bottom}, {duration: 100, queue: false});
          logo_img.animate({'width': '100%'}, {duration: 100, queue: false});
          navigation.animate({'margin-top': original.nav_top}, {duration: 100, queue: false, 
            complete: function(){
              sub_area.css('height', 'auto');
              main_area.removeClass('gdlr-fixed-header');
              
              navigation.removeAttr("style");
              logo.removeAttr("style");
            }
          });               
        }else if( !main_area.hasClass('gdlr-fixed-header') && $(this).width() > 959 &&
          $(this).scrollTop() > main_area.children('.gdlr-header-inner').offset().top - parseInt($('html').css('margin-top')) ){
            sub_area.css('height', main_area.height());
            main_area.addClass('gdlr-fixed-header');
            main_area.children('.top-navigation-wrapper').hide();
            
            logo.animate({'margin-top': '20', 'margin-bottom': '23'}, {duration: 100, queue: false});
            logo_img.animate({'width': '80%'}, {duration: 100, queue: false});
            navigation.animate({'margin-top': '25'}, {duration: 100, queue: false});
        }       
      });
    });     
    
    $(window).trigger('resize');
    $(window).trigger('scroll');
  });

})(jQuery);