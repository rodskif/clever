<?php
/*
Template Name: Шаблон страницы команды
*/
?>

<?php
get_header();
?>

<div class="gdlr-page-title-wrapper instructor">
		<div class="gdlr-page-title-overlay"></div>
		<div class="gdlr-page-title-container container" >
			<h1 class="gdlr-page-title">Instructor List Style 1</h1>
		</div>	
</div>
<section id="content-section-1" >
	<div class="section-container container clearfix">
		<div class="instructor-item-wrapper"  style="margin-bottom: 30px;"  >
			<div class="gdlr-lms-instructor-grid-wrapper clearfix">
				<?php
                    $args = array(
                        "post_type" => 'team',
                        "orderby" => 'ID',
                        "order"   => 'ASC'
                    );
                    $team = new WP_Query($args);
                 	if ($team->have_posts()) { 
                 		$i=0;
                        while ($team->have_posts()) {
                               $team->the_post();
                ?>
				<div class="gdlr-lms-instructor-grid gdlr-lms-col3">
					<div class="gdlr-lms-item clearfix">
						<div class="gdlr-lms-instructor-content">
							<div class="gdlr-lms-instructor-thumbnail">
								<?php $img = get_the_post_thumbnail_url($post->ID); ?>
								<img alt="" src="<?php echo $img; ?>" width="150" height="150">
							</div>
							<div class="gdlr-lms-instructor-title-wrapper">
								<h3 class="gdlr-lms-instructor-title"><?php the_title(); ?></h3>
								<div class="gdlr-lms-instructor-position">
									<?php 
                                    	$pos = get_post_meta($post->ID, '_metaposition_data', true);
                                    ?>
                                    <span><?php echo $pos; ?></span>
								</div>
							</div>
							<div class="gdlr-lms-author-description"><?php the_content(); ?>&hellip;</div>
							<a class="gdlr-lms-button cyan" href="<?php the_permalink() ?>">View Profile</a>
						</div>
					</div>
				</div>
				<?php } } ?>
			</div>
		</div>

		<div class="clear"></div>
	</div>
</section>	

<?php
get_footer();
?>