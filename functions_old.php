<?php
    function e_template_path() {
        echo get_template_directory_uri();
    }

    function change_default_content($content){
        $content = "Это контент записи по умолчанию";
        return $content;
    }
add_filter("default_content", "change_default_content");
 
 //============================Подключаем файлы стилей==================//
 function add_theme_style(){
    //wp_enqueue_style( 'fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css');

    wp_enqueue_style( 'gdlr', get_template_directory_uri() . '/css/gdlr.css');
    wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css');

}

add_action( 'wp_enqueue_scripts', 'add_theme_style' ); 

//=============================Подключаем файлы скриптов================//
function add_theme_scripts(){
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'bootstr', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );


    wp_enqueue_script( 'flexsl', get_template_directory_uri() . '/js/flexslider.js');
}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );  

//================================Подключаем мета теги===================//
function add_theme_metatags(){
    ?>
    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favicon.ico" type="image/x-icon">
    <meta charset="<?php echo get_bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <?php

}

add_action('wp_head', 'add_theme_metatags');

//=========================Регистрация меню============================//
register_nav_menus( array(
    'primary' => 'Главное меню'

) );

//=======================Регистрация сайдбаров===========================//
if ( function_exists('register_sidebar') )
{
    register_sidebars(1, array(
        'id' => 'right',
        'name' => 'Боковая панель',
        'description' => '',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

    register_sidebars(1, array(
        'id'            => 'copyright',
        'name'          => 'Copyright',
        'description'   => '',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<p>',
        'after_title'   => '</p>'
    ));

    register_sidebars(1, array(
        'id' => 'index-insert',
        'name' => 'Панель внутри контента',
        'description' => '',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

}

//======================Регистрация форматов постов====================//
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}

add_action( 'after_setup_theme', 'add_post_formats', 11 );

//======================Регистрация нового типа поста===================//
add_action( 'init', 'register_post_type_slider' ); 
 
function register_post_type_slider() {
 $labels = array(
     'name' => 'Слайдеры',
      'add_new' => 'Добавить новый',
      'all_items' => 'Все слайдеры',
   );
  $args = array(
    'labels' => $labels,
    "description" => "",
    'public' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => true, 
    //'menu_icon' => get_stylesheet_directory_uri() .'/img/function_icon.png', // иконка в меню
    'menu_icon' => 'dashicons-editor-code',
    'menu_position' => 5, // порядок в меню
    'supports' => array( 'title', 'editor', 'author', 'thumbnail')
  );
  register_post_type('slider', $args);
}

add_action( 'init', 'register_post_type_feature' ); 
 
function register_post_type_feature() {
 $labels = array(
     'name' => 'Характеристика',
      'add_new' => 'Добавить новую',
      'all_items' => 'Все характеристики',
   );
  $args = array(
    'labels' => $labels,
    "description" => "",
    'public' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => true, 
    //'menu_icon' => get_stylesheet_directory_uri() .'/img/function_icon.png', // иконка в меню
    'menu_icon' => 'dashicons-awards',
    'menu_position' => 5, // порядок в меню
    'supports' => array( 'title', 'editor', 'author', 'thumbnail')
  );
  register_post_type('feature', $args);
} 


add_action( 'init', 'register_post_type_course' ); 
 
function register_post_type_course() {
 $labels = array(
     'name' => 'Курсы',
      'add_new' => 'Добавить новый',
      'all_items' => 'Все курсы',
   );
  $args = array(
    'labels' => $labels,
    "description" => "",
    'public' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => true, 
    'menu_icon' => 'dashicons-welcome-learn-more',
    'menu_position' => 5, // порядок в меню
    'supports' => array( 'title', 'editor', 'author', 'thumbnail')
  );
  register_post_type('course', $args);
} 

add_action( 'init', 'register_post_type_testimonial' ); 
 
function register_post_type_testimonial() {
 $labels = array(
     'name' => 'Отзывы',
      'add_new' => 'Добавить новый',
      'all_items' => 'Все отзывы',
   );
  $args = array(
    'labels' => $labels,
    "description" => "",
    'public' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => true, 
    'menu_icon' => 'dashicons-format-quote',
    'menu_position' => 6, // порядок в меню
    'supports' => array( 'title', 'editor', 'author', 'thumbnail')
  );
  register_post_type('testimonial', $args);
}


add_action( 'init', 'register_post_type_twit' ); 
 
function register_post_type_twit() {
 $labels = array(
     'name' => 'Твиты',
      'add_new' => 'Добавить новый',
      'all_items' => 'Все твиты',
   );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => true, 
    'menu_icon' => 'dashicons-twitter', // иконка в меню
    'menu_position' => 5, // порядок в меню
    'supports' => array( 'title', 'editor', 'comments', 'author', 'thumbnail')
  );
  register_post_type('twit', $args);
}

add_action( 'init', 'register_post_type_team' ); 
 
function register_post_type_team() {
 $labels = array(
     'name' => 'Инструкторы',
      'add_new' => 'Добавить новый',
      'all_items' => 'Все Инструкторы',
   );
  $args = array(
    'labels' => $labels,
    "description" => "",
    'public' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => true, 
    'menu_icon' => 'dashicons-groups',
    'menu_position' => 6, // порядок в меню
    'supports' => array( 'title', 'editor', 'thumbnail')
  );
  register_post_type('team', $args);
}


//===============Parse twits========
function twit_func() {

  require_once('TwitterAPIExchange.php');
  $settings = array(
      'oauth_access_token' => "3037705701-eYdq2k28d1A8sq4cJwJMA2GhpAl6kvrgkyE1C6Z",
      'oauth_access_token_secret' => "FKEC73XzUdW7QlkQpztC350Up6qC66dshEbRGgtKyjGKe",
      'consumer_key' => "J3EXOMCslgTHYLWgiq01X2yX9",
      'consumer_secret' => "yYKs1Lz9z5dPt68bGWhkrztwdImFrWTXN6sLqWhudprN9ZncxN"
  );

  //$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
  $url = "https://api.twitter.com/1.1/statuses/home_timeline.json";
  $getfield = '?screen_name=@InfoResist&count=20';
  $requestMethod = 'GET';
  $twitter = new TwitterAPIExchange($settings);

  $response = $twitter->setGetfield($getfield)
            ->buildOauth($url, $requestMethod)
            ->performRequest();  

 // echo '<pre>';print_r($response);echo '</pre>';
  $responseDecode = array_reverse(json_decode($response));

  $args_twit = array(
    'numberposts'      => 20,
    'post_type'        => 'twit',
  ); 
  $last_twit_posts = (wp_get_recent_posts( $args_twit ));
  $arrID = [];
  foreach ($last_twit_posts as $last_twit_post){
    $arrID[] = (get_post_meta($last_twit_post['ID'],"twID")[0]);
  }

  if(!empty ($arrID)) $last_twID = max($arrID);
  foreach ($responseDecode as $resp) {  
    if(($resp -> id_str) > $last_twID){  
      $post_data = array(
        'post_title'    => wp_strip_all_tags($resp -> user -> name),
        'post_content'  => $resp -> text,
        'post_excerpt'  => $resp -> text,
        'post_status'   => 'publish',
        'post_type'     => 'twit',                    
      );   
      $post_id = wp_insert_post( $post_data );
      add_post_meta($post_id, 'twID', $resp -> id_str,true);
      add_post_meta($post_id, 'twDate', $resp -> created_at,true);
      add_post_meta($post_id, 'avatarURL', $resp -> user -> profile_image_url,true);
      add_post_meta($post_id, 'imgURL', $resp -> entities -> media[0] -> media_url_https,true);                
    }     
  }
}

add_action('twitter','twit_func');
//do_action('twitter');



//===============================SHORTCODE=========================//
function show_Twitter($atts, $content = null ) {
    $args = array(
        'post_type' => 'twit',
        'orderby' => 'ID',
        'posts_per_page' => (isset($atts['count']) ? $atts['count'] : 5),
        'order' => 'DESC'
    );
    $the_query = new WP_Query( $args );

    $obContent = '';

    if ( $the_query->have_posts() ) {
        ob_start();
       // echo '<div class="well">';
        echo '<h4>'.(isset($atts['title']) ? $atts['title'] : 'Twitter feeds').'</h4>';
        echo '<ul class="list-unstyled">';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $img = get_post_custom_values('avatarURL');
            echo '<li><img src="'.$img[0].'" alt="" style="float:left; padding-right: 10px" />' . get_the_content() . '<br clear="both"><hr /></li>';
        }
        echo '</ul>';
       // echo '</div>';
        $obContent = ob_get_contents();
        ob_end_clean ();
    }
    wp_reset_postdata();

    return $obContent;
}
add_shortcode('showtwitter', 'show_Twitter');

//==============================Widgets====================//
class Purpose_Widget extends WP_Widget
{
   public function __construct(){
       parent::__construct("widget_purpose", "Widget of category purpose",
           array("description" => "Widget with awesome taxonomy purpose")
       );
   }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['cat'] = strip_tags( $new_instance['cat'] );
        $instance['number_post'] = strip_tags( $new_instance['number_post'] );
        return $instance;
    }

    public function form( $instance ) {
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Заголовок</label>
            <input  id="<?php echo $this->get_field_id( 'title' ); ?>" type="text"
                    name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" >
        </p>
        <?php $categories = get_categories(); ?>
        <p>
            <label for="<?php echo $this->get_field_id('cat'); ?>">Выберите рубрику</label>
            <select name="<?php echo $this->get_field_name('cat'); ?>" id="<?php echo $this->get_field_id('cat'); ?>">
                <?php
                foreach ( $categories as $link_cat ) {
                    echo '<option value="' . intval( $link_cat->term_id ) . '"'
                        . selected( $instance['cat'], $link_cat->term_id, false )
                        . '>' . $link_cat->name . "</option>";
                }
                ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'number_post' ); ?>">Количество постов</label>
            <input name="<?php echo $this->get_field_name( 'number_post' ); ?>" type="text"
                   value="<?php echo $instance['number_post']; ?>" >
        </p>


    <?php
    }

    public function widget( $args, $instance) {
        ?>

        <?= $args['before_widget'] ?>

        <section class="color-wrapper">
            <div class="container well">

              <?php $the_query = new WP_Query('posts_per_page='.$instance[ 'number_post' ].'&cat='.$instance[ 'cat' ]);
                while ( $the_query->have_posts() ) { $the_query->the_post(); ?>
                <div class="row">
                    <div class="col-md-9">

                        <h2><?php the_title(); ?></h2>

                        <p><?php the_excerpt(); ?></p>
                    </div>
                    <div class="col-md-3">
                        <a href="<?php the_permalink() ?>" class="btn btn-default btn-lg btn-block">Learn More</a>
                    </div>
                </div>
                <?php }  wp_reset_postdata(); ?>
            </div>
        </section>

        <?= $args['after_widget'] ?>

    <?php
    }

}
add_action("widgets_init", function () {
    register_widget("Purpose_Widget");
});        

//==================================MetaBOX======================//
//Цена//
add_action('add_meta_boxes', 'metaprice_init'); 
add_action('save_post', 'metaprice_save'); 

function metaprice_init() { 
add_meta_box('metaprice', 'Цена курса', 
'metaprice_show', 'course', 'normal', 'default'); 
} 

//Функция рисования метабокса:

function metaprice_show($post, $box) { 

// получение существующих метаданных 
$data = get_post_meta($post->ID, '_metaprice_data', true); 

// скрытое поле с одноразовым кодом 
wp_nonce_field('metaprice_action', 'metaprice_nonce'); 

// поле с метаданными 
echo '<p>Цена: <input type="text" name="metadata_field" value="' 
. esc_attr($data) . '"/></p>'; 
} 

//функция сохранения данных:

function metaprice_save($postID) { 

// пришло ли поле наших данных? 
if (!isset($_POST['metadata_field'])) 
return; 

// не происходит ли автосохранение? 
if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) 
return; 

// не ревизию ли сохраняем? 
if (wp_is_post_revision($postID)) 
return; 

// проверка достоверности запроса 
check_admin_referer('metaprice_action', 'metaprice_nonce'); 

// коррекция данных 
$data = sanitize_text_field($_POST['metadata_field']); 

// запись 
update_post_meta($postID, '_metaprice_data', $data); 

} 


//Должность//
//Цена//
add_action('add_meta_boxes', 'metaposition_init'); 
add_action('save_post', 'metaposition_save'); 

function metaposition_init() { 
add_meta_box('metaposition', 'Должность', 
'metaposition_show', 'team', 'normal', 'default'); 
} 

//Функция рисования метабокса:

function metaposition_show($post, $box) { 

// получение существующих метаданных 
$data = get_post_meta($post->ID, '_metaposition_data', true); 

// скрытое поле с одноразовым кодом 
wp_nonce_field('metaposition_action', 'metaposition_nonce'); 

// поле с метаданными 
echo '<p>Должность: <input type="text" name="position_field" value="' 
. esc_attr($data) . '"/></p>'; 
} 

//функция сохранения данных:

function metaposition_save($postID) { 

// пришло ли поле наших данных? 
if (!isset($_POST['position_field'])) 
return; 

// не происходит ли автосохранение? 
if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) 
return; 

// не ревизию ли сохраняем? 
if (wp_is_post_revision($postID)) 
return; 

// проверка достоверности запроса 
check_admin_referer('metaposition_action', 'metaposition_nonce'); 

// коррекция данных 
$data = sanitize_text_field($_POST['position_field']); 

// запись 
update_post_meta($postID, '_metaposition_data', $data); 

} 


?>