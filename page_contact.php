<?php
/**
 * Template Name: Contact Page
 *
 * 
 */

	// global $post;

	// $name = isset($_REQUEST['contact_form']['name']) ? esc_attr(trim($_REQUEST['contact_form']['name'])) : '';
	// $email = isset($_REQUEST['contact_form']['email']) ? esc_attr(trim($_REQUEST['contact_form']['email'])) : '';
	// $site = isset($_REQUEST['contact_form']['site']) ? esc_attr(trim($_REQUEST['contact_form']['site'])) : '';
	// $message = isset($_REQUEST['contact_form']['message']) ? esc_attr(trim($_REQUEST['contact_form']['message'])) : '';
	// $errors = '';
	// if(isset($_REQUEST['contact_form']['check']) && wp_verify_nonce($_REQUEST['contact_form']['check'], plugin_basename(__FILE__))) {
	// 	if(!$name || !is_email($email) || !$message) {
	// 		$errors = __('Form  has required fields', 'codeus');
	// 	}
	// 	if(!$errors) {
	// 		function set_html_content_type(){
	// 			return 'text/html';
	// 		}
	// 		$errors = -1;
	// 		$headers = array();
	// 		$headers[] = __('From', 'codeus').': '.stripslashes($name).' <'.$email.'>';
	// 		$subject = __('Contact form message', 'codeus');
	// 		$mail_message = '<b>'.__('Name', 'codeus').':</b> '.stripslashes($name).'<br />';
	// 		$mail_message .= '<b>'.__('Email', 'codeus').':</b> '.stripslashes($email).'<br />';
	// 		$mail_message .= '<b>'.__('Site', 'codeus').':</b> '.stripslashes($site).'<br />';
	// 		$mail_message .= '<b>'.__('Message', 'codeus').':</b> '.'<br />'.nl2br(stripslashes($message)).'<br />';
	// 		add_filter( 'wp_mail_content_type', 'set_html_content_type' );
	// 		if(!wp_mail(codeus_get_option('admin_email'), $subject, $mail_message, $headers)) { $errors = __('Sending failed.', 'codeus'); };
	// 		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	// 	}
	// }

	get_header();

 dynamic_sidebar('map'); ?>

<div class="row container">
	<div class="col-md-8">
		<?php echo do_shortcode('[contact-form-7 id="248" title="Contact us"]'); ?>
	</div>
	<div class="col-md-4">
		<?php get_sidebar(); ?>
	</div>

</div>

<div class="below-sidebar-wrapper">
	<section id="content-section-3" class="clearfix">
		<div class="gdlr-parallax-wrapper gdlr-background-image gdlr-show-all no-skin"  id="gdlr-parallax-wrapper-1" data-bgspeed="0.2" style="background-image: url('http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/uploads/2013/11/photodune-4503767-skyline-l.jpg'); padding-top: 100px; padding-bottom: 50px; " >
			<div class="container">
				<div class="row clearfix">
					<div class="col-md-4" >
						<div class="gdlr-box-with-icon-ux gdlr-ux">
							<div class="gdlr-item gdlr-box-with-icon-item pos-top type-circle" >
								<div class="box-with-circle-icon" style="background-color: #e2714d">
									<i class="icon-envelope fa fa-envelope"  style="color:#ffffff;" ></i><br>
								</div>
								<h4 class="box-with-icon-title">Contact By Email</h4>
								<div class="clear"></div>
								<div class="box-with-icon-caption">
									<p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-4" >
						<div class="gdlr-box-with-icon-ux gdlr-ux">
							<div class="gdlr-item gdlr-box-with-icon-item pos-top type-circle" >
								<div class="box-with-circle-icon" style="background-color: #e2714d">
									<i class="icon-phone-sign fa fa-phone"  style="color:#ffffff;" ></i><br>
								</div>
								<h4 class="box-with-icon-title">Contact By Phone</h4>
								<div class="clear"></div>
								<div class="box-with-icon-caption">
									<p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.</p>								
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-4" >
						<div class="gdlr-box-with-icon-ux gdlr-ux">
							<div class="gdlr-item gdlr-box-with-icon-item pos-top type-circle clearfix" >
								<div class="box-with-circle-icon" style="background-color: #e2714d;">
									<i class="icon-home fa fa-home"  style="color:#ffffff;" ></i><br>
								</div>
								<h4 class="box-with-icon-title">Come To See Us</h4>
								<div class="box-with-icon-caption">
										<p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
						</div>
					</div>
				</div> 
			</div>
		</div>
	</section>
</div>


<?php get_footer(); ?>
