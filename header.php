<!DOCTYPE html>
<html lang="<?php echo get_bloginfo( 'language' ); ?>">

<head>
    <title><?php the_title(); ?></title>
    <?php
        wp_head();

        $opt = get_option('wpuniq_theme_options');
        $postid = get_option('header_img');
        $img = wp_get_attachment_image_src($postid, 'full');
    ?>

</head>

<body>

<header class="gdlr-header-wrapper">
        <!-- top navigation -->
                <div class="top-navigation-wrapper">
            <div class="top-navigation-container container">
                <div class="top-navigation-left">   
                    <div class="top-navigation-left-text">
                    <div style="margin: 0px 10px; display: inline-block; *display: inline; *zoom:1;">
<i class="gdlr-icon icon-phone fa fa-phone" style="color: #bababa; font-size: 14px; " ></i> <?php echo $opt[phone]; ?>  
</div>
<div style="margin: 0px 10px ; display: inline-block; *display: inline;  *zoom:1;">
<i class="gdlr-icon icon-envelope fa fa-envelope" style="color: #bababa; font-size: 14px; " ></i><?php echo $opt[email]; ?>
</div>                  </div>
                </div>
                <div class="top-navigation-right">
                    <div class="top-social-wrapper">
                    <div class="social-icon">
<a href="#" target="_blank" >
<i class="fa fa-facebook fa-2x black" aria-hidden="true"></i>    
</a>
</div>
<div class="social-icon">
<a href="#" target="_blank" >
<i class="fa fa-vk fa-2x" aria-hidden="true"></i>
</a>
</div>
<div class="social-icon">
<a href="#" target="_blank" >
<i class="fa fa-google-plus fa-2x" aria-hidden="true"></i>
</a>
</div>
<div class="social-icon">
<a href="#" target="_blank" >
<i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i>
</a>
</div>
<div class="social-icon">
<a href="#" target="_blank" >
<i class="fa fa-tumblr fa-2x" aria-hidden="true"></i>
</a>
</div>
<div class="social-icon">
<a href="#" target="_blank" >
<i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
</a>
</div>
<div class="social-icon">
<a href="#" target="_blank" >
<i class="fa fa-youtube fa-2x" aria-hidden="true"></i>
</a>
</div>
<div class="clear"></div>                   </div>
                    <div class="gdlr-lms-header-signin"><i class="fa fa-lock icon-lock"></i><a data-rel="gdlr-lms-lightbox" data-lb-open="login-form" >Sign In</a><div class="gdlr-lms-lightbox-container login-form">
    <div class="gdlr-lms-lightbox-close"><i class="fa fa-remove icon-remove"></i></div>

    <h3 class="gdlr-lms-lightbox-title">Please sign in first</h3>
    <form class="gdlr-lms-form gdlr-lms-lightbox-form" id="loginform" method="post" action="https://demo.goodlayers.com/clevercourse/wp-login.php">
        <p class="gdlr-lms-half-left">
            <span>Username</span>
            <input type="text" name="log" />
        </p>
        <p class="gdlr-lms-half-right">
             <span>Password</span>
             <input type="password" name="pwd" />
        </p>
        <div class="clear"></div>
        <p class="gdlr-lms-lost-password" >
                        <a href="https://demo.goodlayers.com/clevercourse/wp-login.php?action=lostpassword&redirect_to=http://demo.goodlayers.com/clevercourse" >Lost Your Password?</a>
        </p>
        <p>
            <input type="hidden" name="home_url"  value="http://demo.goodlayers.com/clevercourse" />
            <input type="hidden" name="rememberme"  value="forever" />
            <input type="hidden" name="redirect_to" value="/clevercourse/" />
            <input type="submit" name="wp-submit" class="gdlr-lms-button" value="Sign In!" />
        </p>
    </form>
    <h3 class="gdlr-lms-lightbox-title second-section">Not a member?</h3>
    <div class="gdlr-lms-lightbox-description">Please simply create an account before buying/booking any courses.</div>
    <a class="gdlr-lms-button blue" href="http://demo.goodlayers.com/clevercourse?register=http://demo.goodlayers.com/clevercourse/">Create an account for free!</a>
</div>
<span class="gdlr-separator">|</span><a href="http://demo.goodlayers.com/clevercourse?register=http://demo.goodlayers.com/clevercourse/">Sign Up</a></div>                  <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
</header>

<!-- Navigation -->
<nav class="navbar" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
                <img src="<?php echo $img[0] ?>" widht="<?php echo $img[1] ?>" alt=""  />
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <?php wp_nav_menu( array(
                        'menu'              => 'Главное меню',
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'bs-example-navbar-collapse-1',
                        'menu_class'        => 'nav navbar-nav',
                    )); ?>
        <!-- <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="about.html">About</a>
                </li>
                <li>
                    <a href="services.html">Services</a>
                </li>
                <li>
                    <a href="contact.html">Contact</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Portfolio <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="portfolio-1-col.html">1 Column Portfolio</a>
                        </li>
                        <li>
                            <a href="portfolio-2-col.html">2 Column Portfolio</a>
                        </li>
                        <li>
                            <a href="portfolio-3-col.html">3 Column Portfolio</a>
                        </li>
                        <li>
                            <a href="portfolio-4-col.html">4 Column Portfolio</a>
                        </li>
                        <li>
                            <a href="portfolio-item.html">Single Portfolio Item</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="blog-home-1.html">Blog Home 1</a>
                        </li>
                        <li>
                            <a href="blog-home-2.html">Blog Home 2</a>
                        </li>
                        <li>
                            <a href="/2016/10/12/hello-word/">Blog Post</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Other Pages <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php TEMPLATEPATH. '/page.php' ?>">Our team</a>
                        </li>
                        <li>
                            <a href="sidebar.html">Sidebar Page</a>
                        </li>
                        <li>
                            <a href="faq.html">FAQ</a>
                        </li>
                        <li>
                            <a href="404.html">404</a>
                        </li>
                        <li>
                            <a href="pricing.html">Pricing Table</a>
                        </li>
                    </ul>
                </li>
                <li>
                   <i class="fa fa-search icon-search"></i> 
                </li>
            </ul>
           
        </div> -->
        
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->

</nav>
<div class="clear"></div>
<!-- =============================================================================================================================================== -->


                
      

